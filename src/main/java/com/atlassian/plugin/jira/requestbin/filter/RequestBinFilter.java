package com.atlassian.plugin.jira.requestbin.filter;

import com.atlassian.plugin.jira.requestbin.RequestBin;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

@SuppressWarnings("unused")
public class RequestBinFilter implements Filter {

    private final RequestBin requestBin;

    public RequestBinFilter(RequestBin requestBin) {
        this.requestBin = requestBin;
    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        if (is4RequestBin(httpServletRequest)) {
            captureRequest(httpServletRequest, httpServletResponse, chain);
        } else {
            chain.doFilter(request, response);
        }
    }

    private boolean is4RequestBin(HttpServletRequest httpServletRequest) {
        if (!httpServletRequest.getMethod().toUpperCase().equals("POST")) {
            return false;
        }
        Set<String> parameters = httpServletRequest.getParameterMap().keySet();
        return parameters.contains("requestbin");
    }

    private void captureRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain chain) throws ServletException, IOException {
        ServletInputStream inputStream = httpServletRequest.getInputStream();
        RequestBin.Result result = requestBin.captureMessage(inputStream);
        if (!result.isOK()) {
            httpServletResponse.setStatus(result.getStatusType().getStatusCode());
        } else {
            chain.doFilter(httpServletRequest, httpServletResponse);
        }
    }
}
